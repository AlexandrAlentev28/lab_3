#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring

import re
import sys

import isa
from isa import write_bin_code, write_json_code


def preprocess(raw: str) -> str:
    lines = []
    for line in raw.split("\n"):
        comment_idx = line.find(";")
        if comment_idx != -1:
            line = line[:comment_idx]
        line = line.strip()

        lines.append(line)

    text = " ".join(lines)
    text = re.sub(" +", " ", text)

    return text


def tokenize(text):
    text = re.sub(
        r"'.*'",
        lambda match: f'{",".join(map(lambda char: str(ord(char)), match.group()[1:-1]))}',
        text
    )
    text = re.sub(r"(?<=[^ ]) +(?=:)", "", text)
    text = re.sub(",", " ", text)
    text = re.sub(" +", " ", text)
    tokens = text.split(" ")
    groups = [[]]
    for token in map(str.upper, tokens):
        if not token:
            continue
        if token[-1] == ":" and token[0] != "." \
                or token == 'SECTION' \
                or token in set(map(lambda opcode: opcode.name, isa.Opcode)):
            if token[-1] == ':':
                token = token[:-1]
            if token[0] == '.':
                token = token[1:]
            groups.append([token])
        elif token:
            groups[-1].append(token)
    return groups


def parse(groups):
    data = []
    data_labels = {}
    code = []
    code_labels = {}
    for group in groups:
        if not group:
            continue
        if group[0] != 'SECTION':
            if group[0] in set(map(lambda opcode: opcode.name, isa.Opcode)):
                instruction = {"opcode": group[0], "args": []}
                for arg in group[1:]:
                    if arg in isa.REG_MAP:
                        instruction["args"].append(isa.REG_MAP[arg])
                    else:
                        instruction["args"].append(arg)
                code.append(instruction)
            else:
                if len(group) > 1:
                    data_labels[group[0]] = len(data)
                    data.extend(group[1:])
                else:
                    code_labels[group[0]] = len(code)
    return data, data_labels, code, code_labels


def resolve_labels(data, data_labels, code, code_labels):
    data_labels["INPUT"] = len(data) + len(code)
    data_labels["OUTPUT"] = len(data) + len(code) + 1
    labels = data_labels.copy()
    for label, addr in code_labels.items():
        labels[label] = addr + len(data)
    program = []
    program.extend(data)
    program.extend(code)
    _start = code_labels["_START"] + len(data)
    _interruption = code_labels["_INT"] + len(data) if "_INT" in code_labels \
        else len(data) + len(code) - 1
    for word_idx, word in enumerate(program):
        if isinstance(word, dict):
            for arg_idx, arg in enumerate(word["args"]):
                if arg in code_labels:
                    program[word_idx]["args"][arg_idx] = code_labels[arg] + len(data)
                if arg in data_labels:
                    program[word_idx]["args"][arg_idx] = data_labels[arg]
    return program, _start, _interruption


def translate(text):
    processed_text = preprocess(text)
    token_groups = tokenize(processed_text)
    data, data_labels, code, code_labels = parse(token_groups)
    programs, start, interrupt = resolve_labels(data, data_labels, code, code_labels)

    return programs, start, interrupt


def main(args):
    assert len(args) == 3, \
        "Wrong arguments: translator.py <input_file> <target_json_file> <target_bin_file>"

    source, target_json, target_bin = args

    with open(source, "rt", encoding="utf-8") as file:
        source = file.read()

    programs, start, interrupt = translate(source)

    write_json_code(target_json, programs, start, interrupt)
    byte_count = write_bin_code(target_bin, programs, start, interrupt)

    print(
        f"source LoC: {len(source.split())} code instr: {len(programs)} code bytes: {byte_count}")


if __name__ == '__main__':
    main(sys.argv[1:])
