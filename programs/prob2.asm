  section .data:
  result: 0
  a: 1
  b: 1
  next: 0

  section .text:
      _start:

      calc_loop:
          lwi r1,a
          lwi r2,b
          add r1,r1,r2
          addi r3,zero,4000
          muli r3,r3,1000
          add r2,zero,r3
          bgt r1,r2,calc_end
      body:
          addi r3,zero,next
          sw r3,r1
          addi r1,zero,a
          lwi r2,b
          sw r1,r2
          addi r1,zero,b
          lwi r3,next
          sw r1,r3

          lwi r1,b
          remi r2,r1,2
          bne r2,zero,calc_body_end
          addi r3,zero,result
          lwi r2,result
          lwi r1,next
          add r2,r2,r1
          sw r3,r2

      calc_body_end:
          jmp calc_loop

      calc_end:
          addi r1,zero,10
      discharge_loop:
          lwi r2,result
          bnl r1,r2,discharge_end
          muli r1,r1,10
          jmp discharge_loop
      discharge_end:
          divi r1,r1,10
      print_loop:
          lwi r2,result
          beq r1,zero,print_end
          div r2,r2,r1
          addi r2,r2,48
          addi r3,zero,OUTPUT
          sw r3,r2
          lwi r2,result
          rem r2,r2,r1
          addi r3,zero,result
          sw r3,r2
          divi r1,r1,10

          jmp print_loop
      print_end:
      end:
          halt