  section .data:
    hello: 'Hello, World!',0
  section .text:
      _start:
          addi r2,r0,hello
          addi r3,zero,OUTPUT
      write:
          lw r1,r2
          beq r1,zero,end
          sw r3,r1
          addi r2,r2,1
          jmp write
      end:
          halt