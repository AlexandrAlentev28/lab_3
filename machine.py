#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=invalid-name
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-branches

import json
import logging
from collections import deque
import sys
from typing import Callable

import isa
from isa import Register, decode_opcode, read_bin_code, format_instr, \
    Opcode, Immediate, Instruction


class DataPath:
    memory: list[int]
    program_counter: int
    data_address: int
    data_memory_size: int

    imm_gen: int
    instruction: int
    registers: list[int]

    def __init__(self, memory: list[int], start: int, input_irq: list):
        self.program_counter = start
        self.memory = memory
        self.imm_gen = 0
        self.current_data = 0
        self.registers = [0] * 5
        self.inc_pc = 0
        self.raw_imm = 0
        self.rd = 0
        self.rs1 = 0
        self.rs2 = 0
        self.input_irq: deque[tuple[int, str]] = deque(sorted(input_irq, key=lambda val: val[0]))
        self.output_buffer = deque()
        self.a, self.b = 0, 0
        self.computed = 0
        self.input_addr = len(self.memory)
        self.output_addr = len(self.memory) + 1

    def select_instruction(self) -> int:
        self.instruction = self.memory[self.program_counter]
        self.inc_pc = self.program_counter + 1
        self.rd = Instruction.fetch_rd(self.instruction)
        self.rs1 = Instruction.fetch_rs1(self.instruction)
        self.rs2 = Instruction.fetch_rs2(self.instruction)
        self.raw_imm = self.instruction
        return self.instruction

    def generate_immediate(self, fetch_imm: Callable[[int], int]):
        self.imm_gen = fetch_imm(self.raw_imm)

    def latch_rs1_to_alu(self):
        self.a = self.registers[self.rs1]

    def latch_rs2_to_alu(self):
        self.b = self.registers[self.rs2]

    def latch_imm_to_alu(self):
        """Загружает непосредственное значение в ALU"""
        self.b = self.imm_gen

    def compute(self, opcode: Opcode):
        if opcode in (Opcode.ADD, Opcode.ADDI):
            self.computed = self.a + self.b
        elif opcode in (Opcode.SUB, Opcode.SUBI):
            self.computed = self.a - self.b
        elif opcode in (Opcode.MUL, Opcode.MULI):
            self.computed = self.a * self.b
        elif opcode in (Opcode.DIV, Opcode.DIVI):
            self.computed = self.a // self.b
        elif opcode in (Opcode.REM, Opcode.REMI):
            self.computed = self.a % self.b
        self.computed = int(self.computed)

    def latch_address_to_memory(self):
        if self.registers[self.rs1] == self.input_addr:
            if not self.input_irq:
                raise EOFError
            self.current_data = ord(self.input_irq.popleft()[1])
        else:
            self.current_data = self.memory[self.registers[self.rs1]]

    def store_data_to_memory_from_reg(self):
        if self.registers[self.rs1] == self.output_addr:
            self.output_buffer.append(chr(self.registers[self.rs2]))
        else:
            self.memory[self.registers[self.rs1]] = self.registers[self.rs2]

    def store_data_to_memory_from_imm(self):
        """Загружает данные в память"""
        if self.registers[self.rs1] == self.output_addr:
            self.output_buffer.append(chr(self.imm_gen))
        else:
            self.memory[self.registers[self.rs1]] = self.imm_gen

    def latch_address_to_memory_from_imm(self):
        if self.imm_gen == self.input_addr:
            if not self.input_irq:
                raise EOFError
            self.current_data = ord(self.input_irq.popleft()[1])
        else:
            self.current_data = self.memory[self.imm_gen]

    def latch_rd_from_memory(self):
        """Значение из памяти перезаписывает регистр"""
        if self.rd > 0:
            self.registers[self.rd] = self.current_data

    def latch_rd_from_input(self):
        if self.rd > 0:
            self.registers[self.rd] = ord(self.input_irq.popleft()[1])
        else:
            self.input_irq.popleft()

    def latch_rd_from_inc_pc(self):
        if self.rd > 0:
            self.registers[self.rd] = self.inc_pc

    def latch_rd_from_alu(self):
        """ALU перезаписывает регистр"""
        if self.rd > 0:
            self.registers[self.rd] = self.computed

    def latch_program_counter_from_imm(self):
        """Перезаписывает значение PC из ImmGen"""
        self.program_counter = self.imm_gen

    def latch_program_counter_from_rs2(self):
        self.program_counter = self.registers[self.rs2]

    def latch_program_counter_from_inc(self):
        self.program_counter = self.inc_pc

    def latch_program_counter_from_interrupt_controller(self, interrupt_vector_addr):
        self.program_counter = interrupt_vector_addr

    def latch_program_counter_into_rd(self):
        self.registers[self.rd] = self.program_counter

    def latch_compare_flags(self):
        """Загружает регистры в Branch Comparator."""
        return self.registers[self.rs1] == self.registers[self.rs2],\
            self.registers[self.rs1] < self.registers[self.rs2]


class ControlUnit:
    data_path: DataPath
    opcode: Opcode

    def __init__(self, data_path, interrupt_vector_addr):
        self.data_path = data_path
        self._tick_ = 0
        self.instr = 0
        self.input_irq_handler = interrupt_vector_addr
        self.is_interrupted: bool = False

        self.equals, self.less = False, False

    def current_tick(self):
        return self._tick_

    def tick(self):
        """Счётчик тактов процессора. Вызывается при переходе на следующий такт."""
        logging.debug('%s', self)

        self._tick_ += 1

    def decode_and_execute_instruction(self):
        # Interruption
        if not self.is_interrupted \
                and self.data_path.input_irq \
                and self.data_path.input_irq[0][0] <= self.current_tick():
            self.data_path.latch_program_counter_from_interrupt_controller(self.input_irq_handler)
            self.data_path.rd = 4
            self.data_path.latch_rd_from_inc_pc()
            self.is_interrupted = True
            logging.debug("INTERRUPTION `%s`", self.data_path.input_irq[0][1])
            return

        # Loading instruction
        self.instr = self.data_path.select_instruction()
        self.tick()

        # Decoding instruction

        self.opcode = decode_opcode(self.instr)

        if self.opcode is Opcode.HALT:
            raise StopIteration()

        self.data_path.generate_immediate(
            self.opcode.instruction_type.fetch_imm)
        self.tick()

        # Executing

        self.data_path.latch_rs1_to_alu()

        if self.opcode.instruction_type is Register:
            self.data_path.latch_rs2_to_alu()
        elif self.opcode.instruction_type is Immediate:
            self.data_path.latch_imm_to_alu()
        self.equals, self.less = self.data_path.latch_compare_flags()

        self.data_path.compute(opcode=self.opcode)
        self.tick()
        # Memory access

        if self.opcode is Opcode.LWI:
            self.data_path.latch_address_to_memory_from_imm()
        elif self.opcode is Opcode.LW:
            self.data_path.latch_address_to_memory()
        elif self.opcode is Opcode.SW:
            self.data_path.store_data_to_memory_from_reg()

        elif self.opcode is Opcode.SWI:
            self.data_path.store_data_to_memory_from_imm()
        self.tick()

        # Write back

        if self.opcode in (Opcode.LW, Opcode.LWI):
            self.data_path.latch_rd_from_memory()

        elif self.opcode.instruction_type in (Immediate, Register) \
                and self.opcode not in (Opcode.LW, Opcode.SW):
            self.data_path.latch_rd_from_alu()

        if self.opcode in [Opcode.JAL, Opcode.JALR]:

            if self.opcode is Opcode.JAL:
                self.data_path.latch_program_counter_from_imm()
            else:
                self.data_path.latch_program_counter_from_rs2()
            self.data_path.latch_rd_from_inc_pc()
            self.is_interrupted = False
            self.tick()
            return

        if any([
            self.opcode is Opcode.JMP,
            self.opcode is Opcode.BEQ and self.equals,
            self.opcode is Opcode.BNE and not self.equals,
            self.opcode is Opcode.BLT and self.less,
            self.opcode is Opcode.BNL and not self.less,
            self.opcode is Opcode.BGT and not self.less and not self.equals,
            self.opcode is Opcode.BNG and (self.less or self.equals)
        ]):
            self.data_path.latch_program_counter_from_imm()
        else:
            self.data_path.latch_program_counter_from_inc()
        self.tick()

    def __repr__(self):
        state = f" INTERRUPT: {str(self.is_interrupted):5} -> TICK: {self._tick_:5}" \
                f" PC: {self.data_path.program_counter:5} "
        registers = f"{{[rd: {self.data_path.rd}" \
                    f", rs1: {self.data_path.rs1}" \
                    f", rs2: {self.data_path.rs2}" \
                    f", imm: {self.data_path.imm_gen}]" \
                    f" Regs [{' '.join([str(reg) for reg in self.data_path.registers])}] }}"

        alu = f"ALU [a:{self.data_path.a} b:{self.data_path.b} computed:{self.data_path.computed}]"
        action = f"{isa.format_instr(instr=self.instr)}"
        return f"{state} {registers:35} {alu:20} ~ {action:15}"


def show_memory(memory):
    memory_state = ""
    for address, cell in enumerate(reversed(memory)):
        address = len(memory) - address - 1
        address_br = bin(address)[2:]
        address_br = (10 - len(address_br)) * "0" + address_br
        address_br = (10 - len(address_br)) * "0" + address_br
        cell = int(cell)
        cell_br = bin(cell)[2:]
        cell_br = (32 - len(cell_br)) * "0" + cell_br

        try:
            memory_state += f"({address:5})\
                [{address_br:10}]  -> [{cell_br:32}] = ({cell:10})" f" ~ {format_instr(cell):20}"
        except KeyError:
            memory_state += f"({address:5})\
                        [{address_br:10}]  -> [{cell_br:32}] = ({cell:10})"
        memory_state += "\n"

    return memory_state


def simulation(memory: list[int], start: int,
               interrupt_vector_addr: int, input_tokens: list[tuple[int, str]],
               limit: int):
    """Запуск симуляции процессора.

    Длительность моделирования ограничена количеством выполненных инструкций.
    """
    logging.info("{ INPUT MESSAGE } [ `%s` ]",
                 ", ".join([f"'{char}'[{index}]" for index, char in list(input_tokens)]))
    logging.info("{ INPUT TOKENS  } [ %s ]", ",".join(
        [f"({index},`{char}` as {ord(char)})" for [index, char] in input_tokens]))
    logging.debug("%s", f"Memory map is\n{show_memory(memory)}")
    data_path = DataPath(memory, start, input_tokens)
    control_unit = ControlUnit(data_path, interrupt_vector_addr)
    instruction_counter = 0
    try:
        while True:
            if not limit > instruction_counter:
                print("too long execution, increase limit!")
                break
            control_unit.decode_and_execute_instruction()
            instruction_counter += 1

    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        logging.warning('Stop iteration!')

    finally:
        logging.debug("%s", f"Memory map is\n{show_memory(memory)}")

    return ''.join(data_path.output_buffer), instruction_counter, \
        control_unit.current_tick()


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: machine.py <code.bin> <input>"
    code_file, input_file = args

    memory, start, interrupt_vector_addr = read_bin_code(code_file)

    with open(input_file, encoding="utf-8") as file:
        inputs = json.loads(file.read())
        input_tokens = []
        for i, char in enumerate(inputs["message"]):
            input_tokens.append((inputs["schedule"][i], char))

    output, instr_counter, ticks = simulation(
        memory, start, interrupt_vector_addr,
        input_tokens=input_tokens,
        limit=800
    )

    print(f"Output is `{''.join(output)}`")
    print(f"instr_counter: {instr_counter} ticks: {ticks}")


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
