# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring

import contextlib
import io
import logging
import tempfile

import pytest

import machine
import isa


@pytest.mark.golden_test("golden/unit/machine/*.yml")
def test_whole_by_golden(golden, caplog):
    # Установим уровень отладочного вывода на DEBUG
    caplog.set_level(logging.DEBUG)

    # Создаём временную папку для тестирования приложения.
    with tempfile.TemporaryDirectory():
        with contextlib.redirect_stdout(io.StringIO()):
            data_path = machine.DataPath([], 0, [])
            data_path.registers = [0, int(golden["a"]), int(golden["b"]), 0]
            data_path.rd = 3
            data_path.rs1 = 1
            data_path.rs2 = 2
            data_path.latch_rs1_to_alu()
            data_path.latch_rs2_to_alu()
            opcode = isa.opcodes_by_name[golden["operation"].upper().replace("\n", "")]
            data_path.compute(opcode)
            data_path.latch_rd_from_alu()

        assert data_path.registers[data_path.rd] == int(golden["result"])
