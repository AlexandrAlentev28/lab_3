#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=invalid-name
# pylint: disable=line-too-long


import contextlib
import io
import logging
import os
import tempfile

import pytest

import translator


@pytest.mark.golden_test("golden/unit/translate/*.yml")
def test_whole_by_golden(golden, caplog):
    # Установим уровень отладочного вывода на DEBUG
    caplog.set_level(logging.DEBUG)

    # Создаём временную папку для тестирования приложения.
    with tempfile.TemporaryDirectory() as tmpdirname:
        # Готовим имена файлов для входных и выходных данных.
        source = os.path.join(tmpdirname, "source.asm")
        target_json = os.path.join(tmpdirname, "target.json")
        target_bin = os.path.join(tmpdirname, "target.bin")

        # Записываем входные данные в файлы. Данные берутся из теста.
        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source, target_json, target_bin])

        # Выходные данные также считываем в переменные.
        with open(target_bin, "rb") as file:
            binary_code = file.read()
        with open(target_json, encoding="utf-8") as file:
            struct_code = file.read()

        # Проверяем что ожидания соответствуют реальности.
        assert struct_code == golden.out["struct_code"]
        assert binary_code == golden.out["binary_code"]

        assert stdout.getvalue() == golden.out["output"]
